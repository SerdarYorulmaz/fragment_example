package com.example.example;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    FragmentManager manager; /** manager ilet ransaction islemlerini baslatabiliyoruz **/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager= getSupportFragmentManager(); /** asagidaki tum methodlarda manager kullnabilirim. **/

    }


    public void addFragmentA(View v){

        /** Fragment ekleme */
        FragmentA fragmentA=new FragmentA();

        FragmentTransaction transaction=manager.beginTransaction();
        transaction.add(R.id.container, fragmentA, "fragA");
        transaction.commit();



    }

    public void addFragmentB(View v){

        FragmentB fragmentB=new FragmentB();

        FragmentTransaction transaction=manager.beginTransaction();/** traction mutlaka baslatmak zorundayız **/
        transaction.add(R.id.container, fragmentB, "fragB");
        transaction.commit();

    }

    public void removeFragmentA(View v){
        FragmentA fragmentA= (FragmentA) manager.findFragmentByTag("fragA");
        FragmentTransaction transaction=manager.beginTransaction();
        if(fragmentA!=null) {
            transaction.remove(fragmentA);
            transaction.commit();
        }else{
            Toast.makeText(this,"Fragment A bulunamadı", Toast.LENGTH_LONG).show();
        }

    }

    public void removeFragmentB(View v){
        FragmentB fragmentB= (FragmentB) manager.findFragmentByTag("fragB");
        FragmentTransaction transaction=manager.beginTransaction();
        if(fragmentB!=null) {
            transaction.remove(fragmentB);
            transaction.commit();
        }else{
            Toast.makeText(this,"Fragment B bulunamadı", Toast.LENGTH_LONG).show();
        }

    }

    public void replaceByFragmentA(View v){
            /** yer değiştirme functionu mainActivty deki  frameLoyutta id si container bir fragment
             * var varsa yer degistirecek yerine fragmentA koy
             *  yerine gececek fragment onAttach ve OnCreate function calisip sonra bataki fragment yok edilir.
             * Yerdegistirmede  basta fragment varsa  onu silip yeni fragment koyar(Kendi fragment olsa bile(A varsa a siler tekrar A koyar))
             * Yerdegistirmedede mutlaka TAG ver yok
             *
             * */
        FragmentA fragmentA=new FragmentA();
        FragmentTransaction transaction=manager.beginTransaction();
        transaction.replace(R.id.container, fragmentA, "fragA");
        transaction.commit();

    }

    public void replaceByFragmentB(View v){

        FragmentB fragmentB=new FragmentB();
        FragmentTransaction transaction=manager.beginTransaction();
        transaction.replace(R.id.container, fragmentB, "fragB");
        transaction.commit();

    }

    public void attachFragmentA(View v){

        FragmentA fragmentA= (FragmentA) manager.findFragmentByTag("fragA");
        FragmentTransaction transaction=manager.beginTransaction();
        if(fragmentA!=null)
        {
            transaction.attach(fragmentA); /** detach olan fragment tekrar start,resume haline getiriyor ama dedgimiz gibi fragmen ölmeden oluyor  */
            transaction.commit();
        }
        else{
            Toast.makeText(this,"Fragment A bulunamadı", Toast.LENGTH_LONG).show();
        }


    }

    public void detachFragmentA(View v){

        /**
         *Detach işlemi  Fragment varlığını sürdürür kullandığı layout yok edilir
         * laYOUT YOk edelecek fragment olup olmadığını kontrol etmek gerek.Amac layout yok etmek, fragment i yok etmek degil.
         * */

        FragmentA fragmentA= (FragmentA) manager.findFragmentByTag("fragA");
        FragmentTransaction transaction=manager.beginTransaction();
        if(fragmentA!=null)
        {
            transaction.detach(fragmentA);
            transaction.commit();
        }
        else{
            Toast.makeText(this,"Fragment A bulunamadı", Toast.LENGTH_LONG).show();
        }


    }

    public void showFragmentA(View v){

        /** Fragment hala burda tek degisen visibility */
        FragmentA fragmentA=(FragmentA) manager.findFragmentByTag("fragA");
        FragmentTransaction transaction=manager.beginTransaction();

        if(fragmentA!=null){
            transaction.show(fragmentA);
            transaction.commit();
        }else{
            Toast.makeText(this,"Fragment A bulunamadı", Toast.LENGTH_LONG).show();
        }


    }

    public void hideFragmentA(View v){

        /** fragment gizlemek istiyorsak  onu cagirmamiz gerek
         * Burda herhangi bir life cycle olmuyor sadece fragment görünümü gizleniyor
         * Fragment hala burda tek degisen visibility
         * */

        FragmentA fragmentA=(FragmentA) manager.findFragmentByTag("fragA");
        FragmentTransaction transaction=manager.beginTransaction();

        if(fragmentA!=null){
            transaction.hide(fragmentA);
            transaction.commit();
        }else{
            Toast.makeText(this,"Fragment A bulunamadı", Toast.LENGTH_LONG).show();
        }

    }

}